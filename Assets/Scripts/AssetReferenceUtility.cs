﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AssetReferenceUtility : MonoBehaviour
{
    public AssetReference objectToLoad;                //地址         只掛這個
    public AssetReference accessoryObjectToLoad;       //PREFAB

    public GameObject instantiatedObject;              //虛擬容器
    private GameObject instantiatedAccessoryObject;   //OUTPUT

    private void Start() {
        Addressables.LoadAssetAsync<GameObject>(objectToLoad).Completed+=objectLoadDone;     //傳入objectToLoad，添加FUN
    }

    private void objectLoadDone(AsyncOperationHandle<GameObject> obj)
    {
        if(obj.Status==AsyncOperationStatus.Succeeded)
        {
            GameObject loadedObject=obj.Result;           //不知道哪裡來的容器=傳入OBJ的RESULT
            instantiatedObject=Instantiate(loadedObject);       //INSTAN 
            // if(accessoryObjectToLoad!=null)     //這邊不知道怎麼樣，前面就可以生成了，可以註解掉
            // {
            //     accessoryObjectToLoad.InstantiateAsync(instantiatedObject.transform).Completed+=op=>         //PREFAB INSTANASYC 新任INSTAN=>OP=>傳到instantiatedAccessoryObject
            //     {
            //         if(op.Status==AsyncOperationStatus.Succeeded)
            //         {
            //             instantiatedAccessoryObject=op.Result;
            //         }
            //     };
            // }
        }
    }
}
