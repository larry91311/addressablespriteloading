﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.U2D;

public class AddressableAtlasedSpriteLoader : MonoBehaviour             //use address
{
    public AssetReferenceSprite newSprite;    //使用下面那個發現不行，得掛這個，用同樣模式處理public Atlased&nonAtlased
    


    //public string newSpriteAddress;
    //public bool useAddress;


    //public AssetReferenceAtlasedSprite newAtlasedSprite;
    public string spriteAtlasAddress;
    public string atlasedSpriteName;
    public bool useAtlasedSpriteName;
    private SpriteRenderer spriteRenderer;

    private void Start() {
        spriteRenderer=gameObject.GetComponent<SpriteRenderer>();
        if(useAtlasedSpriteName)
        {
            string atlasedSpriteAddress=spriteAtlasAddress+'['+atlasedSpriteName+']';
            Addressables.LoadAssetAsync<Sprite>(atlasedSpriteAddress).Completed+=SpriteLoaded;         //系統.Load
        }
        else
        {
            //newAtlasedSprite.LoadAssetAsync().Completed+=SpriteLoaded;              //AssetReferenceSprite.Load
            newSprite.LoadAssetAsync<Sprite>().Completed+=SpriteLoaded;  
        }
    }

    private void SpriteLoaded(AsyncOperationHandle<Sprite> obj)
    {
        switch(obj.Status)
        {
            case AsyncOperationStatus.Succeeded:
                spriteRenderer.sprite=obj.Result;
                break;
            case AsyncOperationStatus.Failed:
                Debug.LogError("Sprite load failed.");
                break;
            default:
                //case AsyncOperationStatus.None:
                break;
        }
    }
}
