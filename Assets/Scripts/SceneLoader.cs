﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public AssetReference scene;
    
    public AssetReference firstScene;
    public AsyncOperationHandle<SceneInstance> handle; 

    public AsyncOperationHandle<SceneInstance> firstHandle; 

    public GameObject LoadingMap;

    private bool unloaded;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {    //SINGLE:關掉所有，開新的，轉換場景       ADDITIVE:添加場景，複數場景並存
        Addressables.LoadSceneAsync(scene,UnityEngine.SceneManagement.LoadSceneMode.Single).Completed += SceneLoadCompleted;     //直接LOAD+=
        LoadingMap.SetActive(true);
        
    }

    private void SceneLoadCompleted(AsyncOperationHandle<SceneInstance> obj)
    {
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            Debug.Log("Successfully loaded scene.");
            handle = obj;
            //LoadingMap.SetActive(false);   //場景更換自動銷毀
        }
    }
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && !unloaded)
        {
            unloaded = true;
            UnloadScene();
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            UnloadLastScene();
            
        }
    }

    void UnloadLastScene()
    {
        SceneManager.UnloadScene("SampleScene");
    }

    void UnloadScene()
    {
        Addressables.UnloadSceneAsync(handle, true).Completed += op =>
        {
            if (op.Status == AsyncOperationStatus.Succeeded)
                Debug.Log("Successfully unloaded scene.");
        };
    }
    
}