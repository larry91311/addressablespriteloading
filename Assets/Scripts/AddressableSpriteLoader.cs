﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.U2D;

public class AddressableSpriteLoader : MonoBehaviour             //use address
{
    public AssetReferenceSprite newSprite;
    private SpriteRenderer spriteRenderer;


    public string newSpriteAddress;
    public bool useAddress;

    private void Start() {
        spriteRenderer=gameObject.GetComponent<SpriteRenderer>();
        if(useAddress)
        {
            Addressables.LoadAssetAsync<Sprite>(newSpriteAddress).Completed+=SpriteLoaded;         //系統.Load
        }
        else
        {
            newSprite.LoadAssetAsync<Sprite>().Completed+=SpriteLoaded;              //AssetReferenceSprite.Load
        }
    }

    private void SpriteLoaded(AsyncOperationHandle<Sprite> obj)
    {
        switch(obj.Status)
        {
            case AsyncOperationStatus.Succeeded:
                spriteRenderer.sprite=obj.Result;
                break;
            case AsyncOperationStatus.Failed:
                Debug.LogError("Sprite load failed.");
                break;
            default:
                //case AsyncOperationStatus.None:
                break;
        }
    }
}
